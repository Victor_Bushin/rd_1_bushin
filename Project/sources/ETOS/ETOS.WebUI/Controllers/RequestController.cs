﻿using System.Web.Mvc;

namespace ETOS.WebUI.Controllers
{
    /// <summary>
    /// Контроллер, обеспечивающий работу с заявками на уровне представления.
    /// </summary>
    [Authorize]
    public class RequestController : Controller
    {
        #region Methods

        /// <summary>
        /// Get-версия метода, возвращающая представление с полным перечнем заявок.
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get-версия метода, возвращающая представление для создания новой заявки.
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Get-версия метода, возвращающая представление для просмотра деталей заявки.
        /// </summary>
        [HttpGet]
        public ActionResult Detail()
        {
            return View();
        }

        #endregion
    }
}
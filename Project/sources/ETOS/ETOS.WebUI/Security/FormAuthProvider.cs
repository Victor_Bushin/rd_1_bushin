﻿using System.Web.Security;

using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;

using ETOS.WebUI.Security.Abstract;

namespace ETOS.WebUI.Security
{
    /// <summary>
    /// Класс, реализующий провайдер аутентификации на уровне представлений.
    /// </summary>
    public class FormAuthProvider : IAuthProvider
    {
        #region Fields

        /// <summary>
        /// Экземпляр сервиса учетных записей пользователей.
        /// </summary>
        private IAccountService _accountService;

        #endregion

        #region Constructor

        public FormAuthProvider(IAccountService accountService)
        {
            _accountService = accountService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий аутентификацию пользователя на уровне представлений.
        /// </summary>
        /// <param name="username">Имя пользователя.</param>
        /// <param name="password">Пароль.</param>
        /// <param name="persistent">Флаг запоминания пользователя в системе.</param>
        public bool Authenticate(string username, string password, bool persistent = false)
        {
            var isValid = _accountService.Authorize(
                new DtoAccount
                {
                    Username = username,
                    Password = password,
                    Persistent = persistent
                });
            
            if (isValid)
            {
                FormsAuthentication.SetAuthCookie(username, persistent);
            }

            return isValid;
        }

        #endregion
    }
}
﻿namespace ETOS.WebUI.Security.Abstract
{
    /// <summary>
    /// Интерфейс поставщика аутентификации.
    /// </summary>
    public interface IAuthProvider
    {
        /// <summary>
        /// Метод, реализующий авторизацию пользователя в системе.
        /// </summary>
        /// <param name="username">Имя пользователя (e-mail).</param>
        /// <param name="password">Пароль.</param>
        /// <param name="persistentCookie">Флаг запоминания пользователя в системе.</param>
        bool Authenticate(string username, string password, bool persistent = false);
    }
}

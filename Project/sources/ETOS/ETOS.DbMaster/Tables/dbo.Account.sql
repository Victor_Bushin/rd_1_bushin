﻿CREATE TABLE [dbo].[Account] (
    [AccountId]  INT IDENTITY (1, 1)	NOT NULL,
    [EmployeeId] INT					NOT NULL,
    [Login]      NCHAR (16)				NOT NULL,
    [Password]   NCHAR (20)				NOT NULL,
	CONSTRAINT PK_Accounts_AccountId PRIMARY KEY (AccountId),
	CONSTRAINT FK_Accounts_EmployeeId FOREIGN KEY (EmployeeId)
		REFERENCES [dbo].[Employee](EmployeeId),
   );

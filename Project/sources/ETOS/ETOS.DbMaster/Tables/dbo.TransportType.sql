﻿CREATE TABLE [dbo].[TransportType]
(
	[TransportTypeId]	INT	IDENTITY (1, 1)		NOT NULL, 
    [Name]				NCHAR(30)				NOT NULL,
	CONSTRAINT PK_TransportType_TransportTypeId PRIMARY KEY (TransportTypeId),
)

﻿CREATE TABLE [dbo].[Location]
(
	[LocationId]		INT	IDENTITY (1, 1)		NOT NULL,  
    [Name]				NVARCHAR(150)				NOT NULL, 
    [Address]			NVARCHAR(255)				NULL, 
    [ParentLocationId]	INT						NULL, 
    [IsPOI]				BIT						NOT NULL, 
    [Culture]			INT						NOT NULL
	CONSTRAINT PK_Locations_LocationId PRIMARY KEY (LocationId),
	CONSTRAINT FK_Locations_ParentLocationId FOREIGN KEY (ParentLocationId)
		REFERENCES [dbo].[Location](LocationId),
)
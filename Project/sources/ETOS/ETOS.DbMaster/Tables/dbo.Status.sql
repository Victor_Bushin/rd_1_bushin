﻿CREATE TABLE [dbo].[Status]
(
	[StatusId]	INT	IDENTITY (1, 1)		NOT NULL,
	[Name]		CHAR (30)				NOT NULL,
	CONSTRAINT PK_Statuses_StatusId PRIMARY KEY (StatusId),
)
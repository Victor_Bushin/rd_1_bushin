﻿CREATE TABLE [dbo].[Priority]
(
	[PriorityId]	INT	IDENTITY (1, 1)		NOT NULL, 
    [Name]			NCHAR(30)				NOT NULL,
	CONSTRAINT PK_Priorities_PriorityId PRIMARY KEY (PriorityId),
)
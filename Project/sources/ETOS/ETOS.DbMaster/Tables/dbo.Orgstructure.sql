﻿CREATE TABLE [dbo].[Orgstructure]
(
	[OrgstructureId]		INT	IDENTITY (1, 1)		NOT NULL, 
    [Name]					NCHAR(150)				NOT NULL, 
    [ParentOrgstructureId]	INT						NULL,
	CONSTRAINT PK_Orgstructures_OrgstructuresId PRIMARY KEY (OrgstructureId),
	CONSTRAINT FK_Orgstructures_ParentOrgstructureId FOREIGN KEY (ParentOrgstructureId)
		REFERENCES [dbo].[Orgstructure](OrgstructureId),
)

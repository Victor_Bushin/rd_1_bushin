﻿SET IDENTITY_INSERT dbo.TransportType ON

DECLARE @TransportType TABLE(
    [TransportTypeId] INT            NOT NULL,
    [Name]            NCHAR (50) NOT NULL
	)

INSERT INTO @TransportType ([TransportTypeId], [Name]) VALUES
(1, N'Люкс'),
(2, N'Бизнес-класс'),
(3, N'Стандарт'),
(4, N'Эконм')

MERGE INTO [TransportType] AS [target]
USING @TransportType AS [source] ON [target].[TransportTypeId] = [source].[TransportTypeId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Name] = [source].[Name]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([TransportTypeId], [Name]) 
	VALUES ([source].[TransportTypeId], [source].[Name]);

SET IDENTITY_INSERT dbo.TransportType OFF
GO
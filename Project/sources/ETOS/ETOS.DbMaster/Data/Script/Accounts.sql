﻿SET IDENTITY_INSERT dbo.Account ON

DECLARE @Account TABLE(
    [AccountId]  INT	                NOT NULL,
    [EmployeeId] INT					NOT NULL,
    [Login]      NCHAR (16)				NOT NULL,
    [Password]   NCHAR (20)				NOT NULL
	)

INSERT INTO @Account ([AccountId], [EmployeeId], [Login], [Password]) VALUES
(1, 1, N'Evgeny', N'1234'),
(2, 2, N'Lubov', N'1234')


MERGE INTO [Account] AS [target]
USING @Account AS [source] ON [target].[AccountId] = [source].[AccountId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[EmployeeId] = [source].[EmployeeId],
			[target].[Login] = [source].[Login],
			[target].[Password]	= [source].[Password]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AccountId], [EmployeeId], [Login], [Password]) 
	VALUES ([source].[AccountId], [source].[EmployeeId], [source].[Login], [source].[Password]);

SET IDENTITY_INSERT dbo.Account OFF
GO
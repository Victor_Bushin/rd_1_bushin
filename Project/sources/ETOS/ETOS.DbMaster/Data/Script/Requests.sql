﻿SET IDENTITY_INSERT dbo.Request ON

DECLARE @Request TABLE(
	[RequestId]				INT		                NOT NULL,
	[AuthorId]				INT						NOT NULL,
	[DeparturePoint]		INT						NULL,
	[DestinationPoint]		INT						NULL,
	[DepartureAddress]		CHAR (255)				NOT NULL,
	[DestinationAddress]	CHAR (255)				NOT NULL,
	[DepartureDateTime]		DATETIME				NOT NULL,
	[CreatingDateTime]		DATETIME				NOT NULL,
	[IsBaggage]				BIT						NOT NULL,
	[Comment]				CHAR (255)				NULL, 
    [Mileage]				FLOAT					NOT NULL, 
    [Price]					FLOAT					NOT NULL,
	[StatusId]              INT                     NOT NULL
	)

INSERT INTO @Request ([RequestId], [AuthorId], [DeparturePoint], [DestinationPoint], [DepartureAddress], [DestinationAddress], [DepartureDateTime], [CreatingDateTime], [IsBaggage], [Comment], [Mileage], [Price], [StatusId]) VALUES
(1, 1, 8, NULL, 'Самарская обл. аэропорт Курумоч', 'г.Тольятти Дзержинского 45', '01.11.2016 19:00:00', '01.11.2016 15:00:00', 0, NULL, 0, 0, 2),
(2, 2, 9, 10, 'Тольятти ул.Юбилейная 178', 'Тольятти ул.70 лет октября Автостанция Аврора', '02.11.2016 15:00:00', '02.11.2016 16:00:00', 0, N'Lalala', 0, 0, 2)


MERGE INTO [Request] AS [target]
USING @Request AS [source] ON [target].[RequestId] = [source].[RequestId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[AuthorId] = [source].[AuthorId],
			[target].[DeparturePoint] = [source].[DeparturePoint],
			[target].[DestinationPoint]	= [source].[DestinationPoint],
			[target].[DepartureAddress] = [source].[DepartureAddress],
			[target].[DestinationAddress] = [source].[DestinationAddress],
			[target].[DepartureDateTime]	= [source].[DepartureDateTime],
			[target].[CreatingDateTime] = [source].[CreatingDateTime],
			[target].[IsBaggage] = [source].[IsBaggage],
			[target].[Comment]	= [source].[Comment],
		    [target].[Mileage] = [source].[Mileage],
			[target].[Price] = [source].[Price],
			[target].[StatusId]	= [source].[StatusId]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([RequestId], [AuthorId], [DeparturePoint], [DestinationPoint], [DepartureAddress], [DestinationAddress], [DepartureDateTime], [CreatingDateTime], [IsBaggage], [Comment], [Mileage], [Price], [StatusId]) 
	VALUES ([source].[RequestId], [source].[AuthorId], [source].[DeparturePoint], [source].[DestinationPoint], [source].[DepartureAddress], [source].[DestinationAddress], [source].[DepartureDateTime], [source].[CreatingDateTime], [source].[IsBaggage], [source].[Comment], [source].[Mileage], [source].[Price], [source].[StatusId]);

SET IDENTITY_INSERT dbo.Request OFF
GO
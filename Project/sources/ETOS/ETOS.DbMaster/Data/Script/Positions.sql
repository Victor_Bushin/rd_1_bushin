﻿SET IDENTITY_INSERT dbo.Position ON

DECLARE @Position TABLE(
    [PositionId]      INT           NOT NULL,
    [PositionName]    CHAR (100) NOT NULL,
    [PriorityId]      INT           NOT NULL,
    [IsManager]       BIT           NOT NULL,
    [TransportTypeId] INT           NOT NULL
	)

INSERT INTO @Position ([PositionId], [PositionName], [PriorityId], [IsManager], [TransportTypeId]) VALUES
(1, 'Руководитель', 1, 1, 1),
(2, 'Секретать', 2, 0, 2),
(3, 'Системный администратор', 3, 0, 3),
(4, 'Бухгалтер', 3, 0, 3),
(5, 'Программист', 3, 0, 3),
(6, 'Инженер', 3, 0, 3)

MERGE INTO [Position] AS [target]
USING @Position AS [source] ON [target].[PositionId] = [source].[PositionId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[PositionName] = [source].[PositionName],
			[target].[PriorityId]	= [source].[PriorityId],
			[target].[IsManager]	= [source].[IsManager],
			[target].[TransportTypeId]	= [source].[TransportTypeId]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([PositionId], [PositionName], [PriorityId], [IsManager], [TransportTypeId]) 
	VALUES ([source].[PositionId], [source].[PositionName], [source].[PriorityId], [source].[IsManager], [source].[TransportTypeId]);

SET IDENTITY_INSERT dbo.Position OFF
GO
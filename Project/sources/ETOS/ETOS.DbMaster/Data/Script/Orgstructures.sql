﻿SET IDENTITY_INSERT dbo.Orgstructure ON

DECLARE @Orgstructure TABLE(
    [OrgstructureId]       INT            NOT NULL,
    [Name]                 NCHAR (150) NOT NULL,
    [ParentOrgstructureId] INT            NULL
	)

INSERT INTO @Orgstructure ([OrgstructureId], [Name], [ParentOrgstructureId]) VALUES
(1, N'EPAM', NULL),
(2, N'Самарский регион', 1),
(3, N'Самарский DEV офис ', 2),
(4, N'Software development', 3),
(5, N'Тольяттинский DEV офис', 2),
(6, N'Software development', 5),
(7, N'Московский регион', 1),
(8, N'.Net Framework R&D', 6),
(9, N'Java R&D', 4),
(10, N'.Net Framework R&D', 4),
(11, N'Java R&D', 6),
(12, N'Московский DEV офис ', 7)


MERGE INTO [Orgstructure] AS [target]
USING @Orgstructure AS [source] ON [target].[OrgstructureId] = [source].[OrgstructureId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Name] = [source].[Name],
			[target].[ParentOrgstructureId]	= [source].[ParentOrgstructureId]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([OrgstructureId], [Name], [ParentOrgstructureId]) 
	VALUES ([source].[OrgstructureId], [source].[Name], [source].[ParentOrgstructureId]);

SET IDENTITY_INSERT dbo.Orgstructure OFF
GO
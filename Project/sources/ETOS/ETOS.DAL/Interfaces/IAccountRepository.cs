﻿using ETOS.DAL.Entities;


namespace ETOS.DAL.Interfaces
{
    /// <summary>
    /// Provides specific actions for Account Entity
    /// </summary>
    public interface IAccountRepository : IRepository<Account>
    {
       
    }
}

﻿using ETOS.DAL.Entities;


namespace ETOS.DAL.Interfaces
{
    /// <summary>
    /// Provides specific actions for Request Entity
    /// </summary>
    public interface IRequestRepository : IRepository<Request>
    {

    }
}

﻿
namespace ETOS.DAL
{
    using Entities;
    using System.Data.Entity;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Provides context
    /// </summary>
    public partial class DataBaseModel : DbContext
    {

        /// <summary>
        /// constructor of context uses base constructor
        /// </summary>
        public DataBaseModel() : base("name=DataBaseModel")
        {
            //Database.SetInitializer<DataBaseModel>(new StoreDbInitializer());
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Orgstructure> Orgstructures { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Priority> Priorities { get; set; }
        public virtual DbSet<TransportType> TransportTypes { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }

//        protected override void OnModelCreating(DbModelBuilder modelBuilder)
//        {
/*   modelBuilder.Entity<Account>()
       .Property(e => e.Login)
       .IsFixedLength();

   modelBuilder.Entity<Account>()
       .Property(e => e.Password)
       .IsFixedLength();

   modelBuilder.Entity<Employee>()
       .Property(e => e.FirstName)
       .IsFixedLength();

   modelBuilder.Entity<Employee>()
       .Property(e => e.MiddleName)
       .IsFixedLength();

   modelBuilder.Entity<Employee>()
       .Property(e => e.LastName)
       .IsFixedLength();

   modelBuilder.Entity<Employee>()
       .Property(e => e.Phone)
       .IsFixedLength();

   modelBuilder.Entity<Employee>()
       .Property(e => e.Email)
       .IsFixedLength();
//               */
//    }
}

/// <summary>
/// Class provides initialization of database information
/// </summary>
//public class StoreDbInitializer : DropCreateDatabaseIfModelChanges<DataBaseModel>
//{
//    protected override void Seed(DataBaseModel db)
//    {            

//        var _orgstructureRus = new Orgstructure { Name = "EPAM" };
//        var _orgstructueSamOblast = new Orgstructure { Name = "Самарский регион", ParentOrgstructure = _orgstructureRus };
//        var _orgstructueSam = new Orgstructure { Name = "Самарский DEV офис ", ParentOrgstructure = _orgstructueSamOblast };
//        var _orgstructueSamDev = new Orgstructure { Name = "Software development", ParentOrgstructure = _orgstructueSam };
//        var _orgstructueSamDevJava = new Orgstructure { Name = "Java R&D", ParentOrgstructure = _orgstructueSamDev };
//        var _orgstructueSamDevNet = new Orgstructure { Name = ".Net Framework R&D", ParentOrgstructure = _orgstructueSamDev };
//        var _orgstructueTlt = new Orgstructure { Name = "Тольяттинский DEV офис", ParentOrgstructure = _orgstructueSamOblast };
//        var _orgstructueTltDev = new Orgstructure { Name = "Software development", ParentOrgstructure = _orgstructueTlt };
//        var _orgstructueTltDevJava = new Orgstructure { Name = "Java R&D", ParentOrgstructure = _orgstructueTltDev };
//        var _orgstructueTltDevNet = new Orgstructure { Name = ".Net Framework R&D", ParentOrgstructure = _orgstructueTltDev };
//        var _orgstructueMskOblast = new Orgstructure { Name = "Московский регион", ParentOrgstructure = _orgstructureRus };
//        var _orgstructueMsk = new Orgstructure { Name = "Московский DEV офис ", ParentOrgstructure = _orgstructueMskOblast };
//        var _orgstructueMskDev = new Orgstructure { Name = "Software development", ParentOrgstructure = _orgstructueMsk };
//        var _orgstructueMskDevJava = new Orgstructure { Name = "Java R&D", ParentOrgstructure = _orgstructueMskDev };
//        var _orgstructueMskDevNet = new Orgstructure { Name = ".Net Framework R&D", ParentOrgstructure = _orgstructueMskDev };

//        List<Orgstructure> _newOrgstructures = new List<Orgstructure>
//        {
//            _orgstructureRus,
//            _orgstructueSamOblast,
//            _orgstructueSam,
//            _orgstructueSamDev,
//            _orgstructueSamDevJava,
//            _orgstructueSamDevNet,
//            _orgstructueTlt,
//            _orgstructueTltDev,
//            _orgstructueTltDevJava,
//            _orgstructueTltDevNet,
//            _orgstructueMskOblast,
//            _orgstructueMsk
//        };
//        db.Orgstructures.AddRange(_newOrgstructures);


//        var _rusLoc = new Location { Name = "Россия", Culture = 12, IsPOI = false };
//        var _smroblLoc = new Location { Name = "Самарская область", Culture = 12, IsPOI = false, ParentLocation = _rusLoc };
//        var _mskoblLoc = new Location { Name = "Московская область", Culture = 12, IsPOI = false, ParentLocation = _rusLoc };
//        var _smrLoc = new Location { Name = "Самара", Culture = 12, IsPOI = false, ParentLocation = _smroblLoc };
//        var _mskLoc = new Location { Name = "Москва", Culture = 12, IsPOI = false, ParentLocation = _mskoblLoc };
//        var _tltLoc = new Location { Name = "Тольятти", Culture = 12, IsPOI = false, ParentLocation = _smroblLoc };
//        var _tltEPAMLoc = new Location { Name = "EPAM_TLT", Address = "г.Тольятти ул.Юбилейная 31Е", Culture = 12, IsPOI = true, ParentLocation = _tltLoc };
//        var _smrEPAMLoc = new Location { Name = "EPAM_SMR", Address = "г.Самара ул.Мичурина 21", Culture = 12, IsPOI = true, ParentLocation = _smrLoc };
//        var _mskEPAMLoc = new Location { Name = "EPAM_MSK", Address = "г.Москва ул.Генераторов 6", Culture = 12, IsPOI = true, ParentLocation = _mskLoc };
//        var _airportLoc = new Location { Name = "Аэропорт Курумоч", Address = "Самарская обл. аэропорт Курумоч", Culture = 12, IsPOI = true, ParentLocation = _smroblLoc };
//        var _busLoc = new Location { Name = "Автовокзал Аврора", Address = "Тольятти ул.70 лет октября Автостанция Аврора", Culture = 12, IsPOI = true, ParentLocation = _tltLoc };
//        var _clientLoc = new Location { Name = "компания Рога и Копыта", Address = "Тольятти ул.Юбилейная 178", Culture = 12, IsPOI = true, ParentLocation = _tltLoc };


//        List<Location> _newLocations = new List<Location>

//        {
//            _rusLoc,
//            _smroblLoc,
//            _mskoblLoc,
//            _smrLoc,
//            _mskLoc,
//            _tltLoc,
//            _tltEPAMLoc,
//            _smrEPAMLoc,
//            _mskEPAMLoc,
//            _airportLoc,
//            _busLoc,
//            _clientLoc
//        };
//        db.Locations.AddRange(_newLocations);


//        var _veryFast = new Priority { Name = "Очень срочный" };
//        var _fast = new Priority { Name = "Срочный" };
//        var _normal = new Priority { Name = "Обычный" };

//        List<Priority> _newPriority = new List<Priority> { _veryFast, _fast, _normal };
//        db.Priorities.AddRange(_newPriority);


//        var _vip = new TransportType { Name = "VIP" };
//        var _standart = new TransportType { Name = "Стандарт" };
//        var _busines = new TransportType { Name = "Бизнес класс" };

//        List<TransportType> _newTransportTypesPriority = new List<TransportType> { _vip, _standart, _busines };
//        db.TransportTypes.AddRange(_newTransportTypesPriority);


//        var _pCEO = new Position { PositionName = "Руководитель", IsManager = true, Priority = _veryFast, TransportType = _vip };
//        var _pSecretary = new Position { PositionName = "Секретать", IsManager = false, Priority = _fast, TransportType = _busines };
//        var _pAdmin = new Position { PositionName = "Системный администратор", IsManager = false, Priority = _normal, TransportType = _standart };
//        var _pAccountant = new Position { PositionName = "Бухгалтер", IsManager = false, Priority = _normal, TransportType = _standart };
//        var _pDev = new Position { PositionName = "Программист", IsManager = false, Priority = _normal, TransportType = _standart };
//        var _pEngineer = new Position { PositionName = "Инженер", IsManager = false, Priority = _normal, TransportType = _standart };


//        List<Position> _newPositions = new List<Position> { _pCEO, _pSecretary, _pAdmin, _pAccountant, _pDev, _pEngineer };
//        db.Positions.AddRange(_newPositions);

//        var _emp1 = new Employee { FirstName = "Любовь", LastName = "Викторовна", MiddleName = "Железнова", Email = "Kor@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+79274633746", Position = _pSecretary };
//        var _emp2 = new Employee { FirstName = "Владимир", LastName = "Сергеевич", MiddleName = "Ветров", Email = "Vertor@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+79745333746", Position = _pAdmin };
//        var _emp3 = new Employee { FirstName = "Игорь", LastName = "Степанович", MiddleName = "Сидоров", Email = "Basic@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+79782546746", Position = _pAccountant };
//        var _emp4 = new Employee { FirstName = "Василий", LastName = "Владленович", MiddleName = "Абрасимов", Email = "Landing@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+72763776356", Position = _pDev };
//        var _emp5 = new Employee { FirstName = "Евгений", LastName = "Петрович", MiddleName = "Волоцуев", Email = "Vol@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+72794857346", Position = _pCEO };
//        var _emp6 = new Employee { FirstName = "Глеб", LastName = "Викторович", MiddleName = "Кондратьев", Email = "Kondrl@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+77682394834", Position = _pEngineer };

//        var _newEmployees = new List<Employee> { _emp1, _emp2, _emp3, _emp4, _emp5, _emp6 };
//        db.Employees.AddRange(_newEmployees);


//        var _acc1 = new Account { Employees = _emp1, Login = "Love@mail.ru", Password = "1234" };
//        var _acc2 = new Account { Employees = _emp2, Login = "Vlad@mail.ru", Password = "1234" };
//        var _acc3 = new Account { Employees = _emp3, Login = "Igor@mail.ru", Password = "1234" };
//        var _acc4 = new Account { Employees = _emp4, Login = "Vas@mail.ru", Password = "1234" };
//        var _acc5 = new Account { Employees = _emp5, Login = "Evgeny@mail.ru", Password = "1234" };
//        var _acc6 = new Account { Employees = _emp6, Login = "Gleb@mail.ru", Password = "1234" };
//        var _newAccounts = new List<Account> { _acc1, _acc2, _acc3, _acc4, _acc5, _acc6 };
//        db.Accounts.AddRange(_newAccounts);


//        var _stNeedApprove = new Status { StatusName = "На согласовании" };
//        var _stCarSearch = new Status { StatusName = "Поиск машины" };
//        var _stDeclined = new Status { StatusName = "Отклонена" };
//        var _stUserDeclined = new Status { StatusName = "Отменена пользователем" };
//        var _stAccepted = new Status { StatusName = "Подтверждена" };
//        var _stFinishedFailed = new Status { StatusName = "Завершена неуспешно" };
//        var _stFinishedOk = new Status { StatusName = "Завершена успешно" };
//        var _newStatuses = new List<Status> { _stNeedApprove, _stCarSearch, _stDeclined, _stUserDeclined, _stAccepted, _stFinishedFailed, _stFinishedOk };
//        db.Statuses.AddRange(_newStatuses);

//        var _req1 = new Request { Author = _emp2, CreatingDateTime = new DateTime(2016, 11, 1, 12, 00, 00), DepartureDateTime = new DateTime(2016, 11, 1, 16, 00, 00), DeparturePoint = _emp2.Location, DestinationAddress = "г.Тольятти Дзержинского 45", Comment="Необходимо забрать новое ПО у клиента дома", IsBaggage = false, Mileage = 0, Price = 0 , Status = _stNeedApprove };
//        var _req2 = new Request { Author = _emp3, Comment = "", CreatingDateTime = new DateTime(2016, 11, 2, 13, 00, 00), DepartureDateTime = new DateTime(2016, 11, 2, 14, 00, 00), DeparturePoint = _emp2.Location, DestinationPoint = _airportLoc, IsBaggage = true, Mileage = 0, Price = 0, Status = _stCarSearch  };
//        var _req3 = new Request { Author = _emp4, Comment = "", CreatingDateTime = new DateTime(2016, 11, 2, 14, 00, 00), DepartureDateTime = new DateTime(2016, 11, 2, 17, 00, 00), DeparturePoint = _emp2.Location, DestinationPoint = _clientLoc, IsBaggage = true, Mileage = 20, Price = 100, Status = _stFinishedOk };
//        var _req4 = new Request { Author = _emp5, Comment = "", CreatingDateTime = new DateTime(2016, 11, 1, 15, 00, 00), DepartureDateTime = new DateTime(2016, 11, 1, 19, 00, 00), DeparturePoint = _emp2.Location, DestinationPoint = _busLoc, IsBaggage = false, Mileage = 0, Price = 0 ,Status = _stCarSearch };
//        var _newRequests = new List<Request> { _req1, _req2, _req3, _req4};
//        db.Requests.AddRange(_newRequests);




//        db.SaveChanges();


//    }
//}

}


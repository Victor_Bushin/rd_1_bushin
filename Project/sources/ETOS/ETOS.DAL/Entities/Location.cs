﻿using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ETOS.DAL.Entities
{
    public partial class Location : IEntity
    {
        [Key]
        public int LocationId { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

       
        [StringLength(255)]
        public string Address { get; set; }

        [ForeignKey("ParentLocation")]
        public int? ParentLocationId { get; set; }

        [Required]       
        public bool IsPOI { get; set; }

        [Required]        
        public int Culture { get; set; }


        public virtual Location ParentLocation { get; set; }
       

    }
}

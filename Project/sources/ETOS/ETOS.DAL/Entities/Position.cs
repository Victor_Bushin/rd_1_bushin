﻿using ETOS.DAL.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ETOS.DAL.Entities
{
    public class Position : IEntity
    {


        [Key]
        public int PositionId { get; set; }

        [Required]
        [StringLength(50)]
        public string PositionName { get; set; }

        [ForeignKey("Priority")]
        public int PriorityId { get; set; }

        [Required]
        public bool IsManager { get; set; }

        [ForeignKey("TransportType")]
        public int TransportTypeId { get; set; }

       
        public virtual Priority Priority { get; set; }
        public virtual TransportType TransportType { get; set; }

        
    }
}

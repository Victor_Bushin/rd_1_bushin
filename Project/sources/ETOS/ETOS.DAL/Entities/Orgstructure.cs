﻿using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ETOS.DAL.Entities
{
    public partial class Orgstructure : IEntity
    {



        [Key]
        public int OrgstructureId { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [ForeignKey("ParentOrgstructure")]
        public Nullable<int> ParentOrgstructureId { get; set; }


        public virtual Orgstructure ParentOrgstructure { get; set; }

       
    }
}

﻿
using ETOS.DAL.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ETOS.DAL.Entities
{
    public partial class Request : IEntity
    {
        [Key]
        public int RequestId { get; set; }
        
        [Required]
        [ForeignKey("Author")]
        public int AuthorId { get; set; }

        [ForeignKey("DeparturePoint")]
        public Nullable<int> DeparturePointId { get; set; }

        [ForeignKey("DestinationPoint")]
        public Nullable<int> DestinationPointId { get; set; }

        [StringLength(255)]
        public string DepartureAddress { get; set; }

        [StringLength(255)]
        public string DestinationAddress { get; set; }

        [Required]
        public System.DateTime DepartureDateTime { get; set; }

        [Required]
        public System.DateTime CreatingDateTime { get; set; }

        [Required]
        public bool IsBaggage { get; set; }

        [StringLength(255)]
        public string Comment { get; set; }

        public double Mileage { get; set; }

        public double Price { get; set; }

        [Required]
        [ForeignKey("Status")]
        public int StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual Employee Author { get; set; }
        public virtual Location DeparturePoint { get; set; }
        public virtual Location DestinationPoint { get; set; }
    }
}

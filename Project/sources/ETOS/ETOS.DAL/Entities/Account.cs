namespace ETOS.DAL.Entities
{
    using Interfaces;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Account : IEntity
    {

        [Key]
        public int AccountId { get; set; }

        [ForeignKey("Employees")]
        public int EmployeeId { get; set; }

        [Required]
        [StringLength(30)]
        public string Login { get; set; }

        [Required]
        [StringLength(30)]
        public string Password { get; set; }

        public virtual Employee Employees { get; set; }
    }
}

﻿namespace ETOS.DAL.Entities
{
    using Interfaces;
    using System.ComponentModel.DataAnnotations;


    public partial class Status : IEntity
    {

        [Key]
        public int StatusId { get; set; }

        [Required]
        [StringLength(30)]
        public string StatusName { get; set; }
    }
}

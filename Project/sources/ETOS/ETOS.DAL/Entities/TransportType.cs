﻿using ETOS.DAL.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ETOS.DAL.Entities
{
    public class TransportType : IEntity
    {
        [Key]
        public int TransportTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}

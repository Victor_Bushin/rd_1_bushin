﻿using ETOS.DAL.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ETOS.DAL.Entities
{
    public class Priority : IEntity
    { 
        [Key]
        public int PriorityId { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }
    }
}

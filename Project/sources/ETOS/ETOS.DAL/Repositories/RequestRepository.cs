﻿using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ETOS.DAL.Repositories
{
    public class RequestsRepository : IRequestRepository
    {
        private DataBaseModel db;

        /// <summary>
        /// repository constructor 
        /// </summary>
        /// <param name="context">current database context</param>
        public RequestsRepository(DataBaseModel context)
        {
            this.db = context;
        }

        /// <summary>
        /// Creates new item in Requests
        /// </summary>
        /// <param name="item">item</param>
        public void Create(Request item)
        {
            db.Requests.Add(item);
        }

        /// <summary>
        /// Removes item by id
        /// </summary>
        /// <param name="id">id of item</param>
        public void Delete(int id)
        {
            Request req = db.Requests.Find(id);
            if (req != null)
                db.Requests.Remove(req);
        }

        /// <summary>
        /// Returns list of Requests using conditions
        /// </summary>
        /// <param name="predicate">conditions</param>
        /// <returns>List of Requests</returns>
        public IEnumerable<Request> Find(Func<Request, bool> predicate)
        {
            return db.Requests.Where(predicate).ToList();
        }

        /// <summary>
        /// Returns Item using specified ID
        /// </summary>
        /// <param name="id">id of item</param>
        /// <returns></returns>
        public Request Get(int id)
        {
            return db.Requests.Find(id);
        }

        /// <summary>
        /// Returns Requests entity
        /// </summary>
        /// <returns>Requests dbset</returns>
        public IEnumerable<Request> GetAll()
        {
            return db.Requests;
        }

        /// <summary>
        /// Sets item as modified 
        /// </summary>
        /// <param name="item">item </param>
        public void Update(Request item)
        {
            db.Entry(item).State = EntityState.Modified;
        }




        private bool disposed = false;

        /// <summary>
        /// Disposing database
        /// </summary>
        /// <param name="disposing">if dispose need flag</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Disposing
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Saves changes to database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }
    }
}

﻿
using ETOS.DAL.Interfaces;
using ETOS.DAL.Entities;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using ETOS.DAL;

namespace DAL.Repositories
{
    /// <summary>
    /// Class for Employee entity interaction
    /// </summary>
    public class EmployeesRepository : IRepository<Employee>
    {

        private DataBaseModel db;
       

        /// <summary>
        /// repository constructor 
        /// </summary>
        /// <param name="context">current database context</param>
        public EmployeesRepository(DataBaseModel context)        
        {
            this.db = context;
        }

        /// <summary>
        /// Creates new item in Employee
        /// </summary>
        /// <param name="item">item</param>
        public void Create(Employee item)
        {
            db.Employees.Add(item);            
        }



        /// <summary>
        /// Removes item by id
        /// </summary>
        /// <param name="id">id of item</param>
        public void Delete(int id)
        {
            Employee acc = db.Employees.Find(id);
            if (acc != null)
                db.Employees.Remove(acc);
                    
        }

        /// <summary>
        /// Returns list of Employees using conditions
        /// </summary>
        /// <param name="predicate">conditions</param>
        /// <returns>List of Employees</returns>
        public IEnumerable<Employee> Find(Func<Employee, bool> predicate)
        {
            return db.Employees.Where(predicate).ToList();
        }

        /// <summary>
        /// Returns Item using specified ID
        /// </summary>
        /// <param name="id">id of item</param>
        /// <returns></returns>
        public Employee Get(int id)
        {
            return db.Employees.Find(id);
        }

        /// <summary>
        /// Returns accounts entity
        /// </summary>
        /// <returns>Accounts dbset</returns>
        public IEnumerable<Employee> GetAll()
        {
            return db.Employees;
        }

        /// <summary>
        /// Sets item as modified 
        /// </summary>
        /// <param name="item">item </param>
        public void Update(Employee item)
        {
            db.Entry(item).State = EntityState.Modified;         
        }




        private bool disposed = false;

        /// <summary>
        /// Disposing database
        /// </summary>
        /// <param name="disposing">if dispose need flag</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Disposing
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Saves changes to database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }
    }
}

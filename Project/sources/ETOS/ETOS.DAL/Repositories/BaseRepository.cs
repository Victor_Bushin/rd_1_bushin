﻿using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ETOS.DAL.Repositories
{
    /// <summary>
    /// Class provides implementation common operations for entities and some common actions for all entity repositories
    /// </summary>
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : IEntity 
    {

        private DataBaseModel db;

        

        /// <summary>
        /// constructor connect database context
        /// </summary>
        public BaseRepository(DataBaseModel context)
        {
            this.db = context;
        }

        /// <summary>
        /// Creates new item in entity
        /// </summary>
        /// <param name="item">item to create</param>
        public void Create(TEntity item)
        {
           
            //db.Tdbset.Accounts();
        }

      
     
        /// <summary>
        /// Delete item from entity by specified ID
        /// </summary>
        /// <param name="id">id of entity</param>
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Search item in entity and return it
        /// </summary>
        /// <param name="predicate">conditions for search</param>
        /// <returns>IEnumerable result</returns>
        public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get data using queries
        /// </summary>
        /// <returns>IQueryable data</returns>
        public IQueryable<TEntity> Get()
        {
            throw new NotImplementedException();
        }

        public TEntity Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all elements
        /// </summary>
        /// <returns>IEnumerable elements</returns>
        public IEnumerable<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            db.SaveChanges();

        }

        /// <summary>
        /// Update item in entity
        /// </summary>
        /// <param name="item">item to update</param>
        public void Update(TEntity item)
        {
        }
    }
}

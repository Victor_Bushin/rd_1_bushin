﻿using ETOS.DAL.Interfaces;
using ETOS.DAL.Entities;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ETOS.DAL.Repositories
{
    /// <summary>
    /// Class for Account entity interaction    
    /// </summary>
    public class AccountsRepository : IAccountRepository
    {

        private DataBaseModel db;

        /// <summary>
        /// repository constructor 
        /// </summary>
        /// <param name="context">current database context</param>
        public AccountsRepository(DataBaseModel context)
        {
            this.db = context;
        }

        /// <summary>
        /// Creates new item in Accounts
        /// </summary>
        /// <param name="item">item</param>
        public void Create(Account item)
        {
            db.Accounts.Add(item);
        }

        /// <summary>
        /// Removes item by id
        /// </summary>
        /// <param name="id">id of item</param>
        public void Delete(int id)
        {
            Account acc = db.Accounts.Find(id);
            if (acc != null)
                db.Accounts.Remove(acc);
        }

        /// <summary>
        /// Returns list of Accounts using conditions
        /// </summary>
        /// <param name="predicate">conditions</param>
        /// <returns>List of Accounts</returns>
        public IEnumerable<Account> Find(Func<Account, bool> predicate)
        {
            return db.Accounts.Where(predicate).ToList();
        }

        /// <summary>
        /// Returns Item using specified ID
        /// </summary>
        /// <param name="id">id of item</param>
        /// <returns></returns>
        public Account Get(int id)
        {
            return db.Accounts.Find(id);
        }

        /// <summary>
        /// Returns accounts entity
        /// </summary>
        /// <returns>Accounts dbset</returns>
        public IEnumerable<Account> GetAll()
        {
            return db.Accounts;
        }

        /// <summary>
        /// Sets item as modified 
        /// </summary>
        /// <param name="item">item </param>
        public void Update(Account item)
        {
            db.Entry(item).State = EntityState.Modified;
        }




        private bool disposed = false;

        /// <summary>
        /// Disposing database
        /// </summary>
        /// <param name="disposing">if dispose need flag</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Disposing
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Saves changes to database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }
    }
}

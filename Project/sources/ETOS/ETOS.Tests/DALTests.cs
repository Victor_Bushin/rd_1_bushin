﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ETOS.DAL.Entities;
using ETOS.DAL;
using System.Linq;
using DAL.Repositories;

namespace ETOS.Tests
{
    [TestClass]
    public class DALTests
    {

        [TestMethod]
        // Проверка что данные записываются в БД и правильно читаются
        // Создание записи в таблице Accounts и Employee
        public void TestCreateUser()
        {
            //arrange
            var db = new DataBaseModel();          

            EmployeesRepository Employee = new EmployeesRepository(db);

            var newOrgstructue = db.Orgstructures.Find(1);
            var newLocation = db.Locations.Find(1);
            var newPriority = db.Priorities.Find(1);
            var newTransportType = db.TransportTypes.Find(1);
            var newPosition = db.Positions.Find(1);

            var expectedName = "Егор";
            var expectedLastName = "Квасов";
            var newEmployee = new Employee { Email = "ceo@epam.com", FirstName = expectedName, LastName = expectedLastName, MiddleName = "Напиткович", Phone = "+79276493847", Location = newLocation, Orgstructure = newOrgstructue, Position = newPosition };


            //act   
            Employee.Create(newEmployee);
            Employee.Save();

            var result = db.Employees.Find(newEmployee.EmployeeId);


            // assert
            Assert.AreEqual(expectedName, result.FirstName);
            Assert.AreEqual(expectedLastName, result.LastName);

        }

        [TestMethod]
        // Проверка что данные удаляются из БД
        public void TestFindDeleteMethod()
        {
            //arrange
            var db = new DataBaseModel();
         

            EmployeesRepository Employee = new EmployeesRepository(db);

            //act   
            var newEmployee = Employee.Find(emp => (emp.FirstName == "Егор")).FirstOrDefault();
            
            if (newEmployee != null)
            {
                int id = newEmployee.EmployeeId;
                Employee.Delete(id);                
                Employee.Save();    
            }

            newEmployee = Employee.Find(emp => (emp.FirstName == "Егор")).FirstOrDefault();

            // assert
            Assert.AreEqual(null, newEmployee);


        }
    }
}

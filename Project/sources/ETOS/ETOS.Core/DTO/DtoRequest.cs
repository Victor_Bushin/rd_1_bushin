﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ETOS.DAL.Entities;

namespace ETOS.Core.DTO
{
    /// <summary>
    /// Объект передачи данных между моделью уровня представления и уровнем доступа к данным.
    /// Представляет собой аккаунт пользователя.
    /// </summary>
    public class DtoRequest
    {
        /// <summary>
        /// Идентефикационный номер заявки
        /// </summary>
        public int DtoRequestId { get; set; }

        /// <summary>
        /// Идентефикационный номер автора заявки
        /// </summary>
        public int DtoAuthorId { get; set; }

        /// <summary>
        /// Код точки отправки
        /// </summary>
        public Nullable<int> DtoDeparturePointId { get; set; }

        /// <summary>
        /// Код точки прибытия
        /// </summary>
        public Nullable<int> DtoDestinationPointId { get; set; }

        /// <summary>
        /// Адрес точки отправки
        /// </summary>
        public string DtoDepartureAddress { get; set; }
        /// <summary>
        /// Адрес точки прибытия
        /// </summary>
        public string DtoDestinationAddress { get; set; }
        /// <summary>
        /// Время отправки
        /// </summary>
        public DateTime DtoDepartureDateTime { get; set; }
        /// <summary>
        /// Время прибытия
        /// </summary>
        public DateTime DtoCreatingDateTime { get; set; }
        /// <summary>
        /// Наличие багажа,да/нет
        /// </summary>
        public bool DtoIsBaggage { get; set; }
        /// <summary>
        /// Комментарии к заявке
        /// </summary>
        public string DtoComment { get; set; }
        /// <summary>
        /// Километраж
        /// </summary>
        public double DtoMileage { get; set; }
        /// <summary>
        /// Общая сумма за поездку
        /// </summary>
        public double DtoPrice { get; set; }
        /// <summary>
        /// Идентефикационный номер статуса
        /// </summary>
        public int DtoStatusId { get; set; }        
    }
}

﻿using System.Linq;

using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;

using ETOS.DAL.Interfaces;

namespace ETOS.Core.Services
{
    /// <summary>
    /// Сервис, реализующий функционал для работы с учетными данными пользователей системы на уровне бизнес-логики.
    /// </summary>
    public class AccountService : IAccountService
    {
        #region Fields

        /// <summary>
        /// Экземпляр репозитория аккаунтов.
        /// </summary>
        private IAccountRepository _accountRepository;

        #endregion

        #region Constructor

        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий авторизацию пользователя в системе.
        /// </summary>
        /// <param name="username">Логин пользователя.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>true - в случае успешной авторизации, false - в противном случае. </returns>
        public bool Authorize(DtoAccount account)
        {
            return (_accountRepository
                   .Find(acc => (acc.Login == account.Username && acc.Password == account.Password))
                   .FirstOrDefault()) != null;
        }
        #endregion
    }
}

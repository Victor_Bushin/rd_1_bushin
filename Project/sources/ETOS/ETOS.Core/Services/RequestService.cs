﻿using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;
using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace ETOS.Core.Services
{
    /// <summary>
    /// Сервис, реализующий функционал для работы с заявками пользователей системы на уровне бизнес-логики.
    /// </summary>
    class RequestService : IRequestService
    {
        #region Fields

        /// <summary>
        /// Экземпляр репозитория заявок.
        /// </summary>
        private IRequestRepository _requestRepository;

        #endregion

        #region Constructor

        public RequestService(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод,реализующий получение списка заявок с учётом фильтрации.
        /// </summary>
        /// <param name="filter">Условие фильтрования.</param>
        public IEnumerable<DtoRequest> GetRequest(IFilter filter)
        {
            var filterList = _requestRepository.Find(x => (filter.RequestId == 0) || (x.RequestId == filter.RequestId));
            filterList.Where(x => (filter.AuthorId == 0) || (x.AuthorId == filter.AuthorId)).
                       Where(x => (filter.DeparturePointId == null) || (x.DeparturePointId == filter.DeparturePointId)).
                       Where(x => (filter.DestinationPointId == null) || (x.DestinationPointId == filter.DestinationPointId)).
                       Where(x => (filter.DepartureAddress == null) || (x.DepartureAddress == filter.DepartureAddress)).
                       Where(x => (filter.DestinationAddress == null) || (x.DestinationAddress == filter.DestinationAddress)).
                       Where(x => (filter.DepartureDateTime == null) || (x.DepartureDateTime == filter.DepartureDateTime)).
                       Where(x => (filter.CreatingDateTime == null) || (x.CreatingDateTime == filter.CreatingDateTime)).
                       Where(x => (filter.IsBaggage == false) || (x.IsBaggage == filter.IsBaggage)).
                       Where(x => (filter.Comment == null) || (x.Comment == filter.Comment)).
                       Where(x => (filter.Mileage == 0) || (x.Mileage == filter.Mileage)).
                       Where(x => (filter.Price == 0) || (x.Price == filter.Price)).
                       Where(x => (filter.StatusId ==0) || (x.StatusId == filter.StatusId));
            var outList = Mapper.Map<Request, IEnumerable<DtoRequest>>(filterList.ElementAt(0));

            for (int i = 1; i < filterList.Count(); i++)
            {
                outList = Mapper.Map<Request, IEnumerable<DtoRequest>>(filterList.ElementAt(i));
            }

            return outList;            
        }

        /// <summary>
        /// Метод отрабатывает вызов реализации добавления заявки через репозиторий
        /// </summary>
        /// <param name="item"></param>
        public void AddRequest(DtoRequest item)
        {
            var createItem = Mapper.Map<DtoRequest, Request>(item);
            _requestRepository.Create(createItem);
        }

        /// <summary>
        /// Метод отрабатывает вызов реализации удаления заявки через репозиторий
        /// </summary>
        /// <param name="item"></param>
        public void DeleteRequest(DtoRequest item)
        {
            var deleteItem = Mapper.Map<DtoRequest, Request>(item);
            _requestRepository.Delete(deleteItem.RequestId);
        }

        /// <summary>
        /// Метод отрабатывает вызов реализации сохранения изменений в репозитории
        /// </summary>
        public void SaveChanges()
        {
            _requestRepository.Save();
        }
        #endregion
    }
}

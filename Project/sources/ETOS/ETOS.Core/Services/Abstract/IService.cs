﻿using ETOS.DAL.Interfaces;

namespace ETOS.Core.Services.Abstract
{
    /// <summary>
    /// Базовый интерфейс, имлементируемый каждым сервисом уровня бизнес-логики.
    /// </summary>
    /// <typeparam name="TEntity">Сущность, с которой работает сервис.</typeparam>
    public interface IService<TEntity> where TEntity : IEntity { }
}

﻿using ETOS.DAL.Entities;

namespace ETOS.Core.Services.Abstract
{
    /// <summary>
    /// Интерфейс сервиса по работе с заявками пользователей.
    /// </summary>
    public interface IRequestService : IService<Request>
    {

    }
}

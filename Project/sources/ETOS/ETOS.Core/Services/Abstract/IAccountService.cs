﻿using ETOS.Core.DTO;
using ETOS.DAL.Entities;

namespace ETOS.Core.Services.Abstract
{
    /// <summary>
    /// Интерфейс сервиса по работе с учетными записями пользователями.
    /// </summary>
    public interface IAccountService : IService<Account>
    {
        /// <summary>
        /// Метод, реализующий авторизацию пользователя в системе.
        /// </summary>
        /// <param name="account">Учетная запись для авторизации.</param>
        /// <returns>true - в случае успешной авторизации, false - в противном случае. </returns>
        bool Authorize(DtoAccount account);
    }
}
